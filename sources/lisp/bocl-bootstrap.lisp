;;;; -*- mode:lisp;coding:iso-8859-1 -*-

;;;; ============================================================
;;;; BOCL implementation over the BOCL kernel.
;;;; ============================================================

;;; This file is a script loaded over the bare kernel.
;;; It is read with the kernel reader.
;;; In the KERNEL obarray.
;;;
;;; We don't have package yet.
;;;
;;;

;; macros

;; symbols
;; packages
;; structures
;; list
;; sequence
;; array
;; reader
;; loop
;; clos-mop
;; gray-streams
;; formatter
;; printer
;; debugger


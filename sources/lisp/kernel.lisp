(cl:defpackage "COM.INFORMATIMAGO.BOCL.KERNEL"
  (:nicknames "KERNEL")
  (:use #|Nothing|#)
  (:shadow
   . #9=(
         "KERNEL-ROOTSET" "GET-EVAL-COUNTERS" "CLASS-NEW" "CLASSP" "CLASS-NAME"
         "CLASS-DIRECT-SUPERCLASSES" "CLASS-ADD-SUPERCLASS" "CLASS-OF" "FIND-CLASS"
         "SUBCLASSP" "TYPEP" "CHECK-TYPE" "CHECK-CLASS" "BUILT-IN-CLASS-NEW" "CONS"
         "CONSP" "CONS-CAR" "CONS-CDR" "CONS-RPLACA" "CONS-RPLACD" "INTEGERP"
         "FLOATP" "CHARACTERP" "CHAR-CODE" "CHAR-INT" "CHAR-NAME" "CODE-CHAR"
         "NAME-CHAR" "FIND-SYMBOL" "MAKE-SYMBOL" "INTERN" "SYMBOL-NAME"
         "SYMBOL-PACKAGE" "SYMBOL-VALUE" "SYMBOL-FUNCTION" "SYMBOL-PLIST"
         "SET-SYMBOL-PACKAGE" "SET-SYMBOL-VALUE" "SET-SYMBOL-FUNCTION"
         "SET-SYMBOL-PLIST" "SYMBOLP" "BOUNDP" "FBOUNDP" "MAKUNBOUND" "FMAKUNBOUND"
         "INTERN-KEYWORD" "KEYWORDP" "OBARRAY-ALL-SYMBOLS" "NULL"
         "NORMALIZE-EXTERNAL-FORMAT" "PHYSICAL-PATHNAME" "PHYSICAL-PATHNAME-EQUAL"
         "PHYSICAL-PATHNAME-P" "PATHNAMEP" "MAKE-FILE-STREAM" "STREAM-PATHNAME"
         "STREAM-ELEMENT-TYPE" "STREAM-EXTERNAL-FORMAT" "STREAM-DIRECTION"
         "OPEN-STREAM-P" "STANDARD-INPUT-STREAM" "STANDARD-OUTPUT-STREAM"
         "STANDARD-ERROR-STREAM" "CLOSUREP" "FUNCTIONP" "BUILT-IN-FUNCTION-P"
         "FUNCTION-NAME" "MAKE-FUNCTION" "MAKE-CLOSURE" "RPLACA" "RPLACD" "CAR" "CDR"
         "CAAR" "CADR" "CDAR" "CDDR" "CAAAR" "CAADR" "CADAR" "CADDR" "CDAAR" "CDADR"
         "CDDAR" "CDDDR" "CAAAAR" "CAAADR" "CAADAR" "CAADDR" "CADAAR" "CADADR"
         "CADDAR" "CADDDR" "CDAAAR" "CDAADR" "CDADAR" "CDADDR" "CDDAAR" "CDDADR"
         "CDDDAR" "CDDDDR" "LIST-LENGTH" "LIST-POSITION" "NCONC" "LAST" "COPY-LIST"
         "LIST" "DELETE" "NREVERSE" "REVERSE" "MEMQ" "ASSQ" "FIRSTS" "GETF" "PUTF"
         "REMF" "VECTOR" "VECTORP" "VECTOR-LENGTH" "SVREF" "SVREF-SET" "BIT-VECTOR-P"
         "BIT-VECTOR-LENGTH" "BIT" "SBIT" "BIT-SET" "SBIT-SET" "BIT-VECTOR-EQUAL"
         "STRING-LENGTH" "STRING=" "STRING-EQUAL" "STRINGP" "CHAR" "SCHAR" "SET-CHAR"
         "SET-SCHAR" "EQL" "EQUAL" "VALUES" "VALUES-LIST" "PATHNAME" "NAMESTRING"
         "PRIN1-TO-STRING" "PRINT" "PRIN1" "PRINC" "TERPRI" "KERNEL-READ-CHAR"
         "KERNEL-PEEK-CHAR" "KERNEL-READ" "KERNEL-READ-PRESERVING-WHITESPACE"
         "KERNEL-READ-DELIMITED-LIST" "KERNEL-READ-FROM-STRING"
         "MAKE-STRING-INPUT-STREAM" "MAKE-STRING-OUTPUT-STREAM"
         "GET-OUTPUT-STREAM-STRING" "OPEN" "FINISH-OUTPUT" "FORCE-OUTPUT" "=/I" "=/F"
         "MAKE-STRUCTURE-CLASS" "STRUCTURE-CLASS-INSTANCE-SIZE"
         "STRUCTURE-CLASS-INSTANCE-DIRECT-SLOTS" "STRUCTURE-CLASS-INSTANCE-SLOTS"
         "MAKE-STRUCTURE-INSTANCE" "STRUCTUREP" "STRUCTURE-GET" "STRUCTURE-SET"
         "MAKE-ENVIRONMENT" "ENVIRONMENT-IS-TOPLEVEL" "ENVIRONMENT-VARIABLES"
         "ENVIRONMENT-FUNCTIONS" "ENVIRONMENT-BLOCKS" "ENVIRONMENT-CATCHS"
         "ENVIRONMENT-DECLARATIONS" "ENVIRONMENT-NEXT-ENVIRONMENT"
         "ENVIRONMENT-FRAMES" "ENVIRONMENT-ADD-VARIABLE" "ENVIRONMENT-ADD-FUNCTION"
         "ENVIRONMENT-ADD-BLOCK" "ENVIRONMENT-ADD-CATCH"
         "ENVIRONMENT-ADD-DECLARATION" "ENVIRONMENT-FIND-VARIABLE"
         "ENVIRONMENT-FIND-FUNCTION" "ENVIRONMENT-FIND-BLOCK"
         "ENVIRONMENT-FIND-CATCH" "ENV-SPECIAL-VARIABLE-P"))
  (:export . #9#)


  ;; used by eval-when:
  (:export "COMPILE" "LOAD" "EVAL"
           "*LOAD-PATHNAME*" "*LOADING-FASL-FILE*")


  (:export
   "$CONSTANT-VARIABLE" "$SPECIAL" "$NOTINLINE"
   "$COMPILER-MACRO-FUNCTION"
   "$SYMBOL-MACRO-EXPANSION"
   "$TYPE" "$UNBOUND")

  (:export
   "BINDINGS-AREF" "BINDINGS-FREF"  "BINDINGS-VREF"
   "BINDINGS-NBINDINGS" "BINDINGS-NFUNS" "BINDINGS-NVARS" "MAKE-BINDINGS")

  (:export
   "*FRAME*"

   "FRAME" "FRAMEP" "MAKE-FRAME"
   "FRAME-NEXT" "FRAME-TARGET" "FRAME-BINDINGS"
   "FRAME-RESULTS" "FRAME-NON-LOCAL-EXIT"
   "PROTECT-FRAME" "PROTECT-FRAME-P" "MAKE-PROTECT-FRAME"
   "BLOCK-FRAME"   "BLOCK-FRAME-P" "MAKE-BLOCK-FRAME"
   "BLOCK-FRAME-NAME"
   "CATCH-FRAME"   "CATCH-FRAME-P" "MAKE-CATCH-FRAME"
   "CATCH-FRAME-OBJECT"
   "TAGBODY-FRAME" "TAGBODY-FRAME-P" "MAKE-TAGBODY-FRAME"
   "TAGBODY-FRAME-TAGS" "TAGBODY-FRAME-TARGET-TAG"
   "HANDLER-FRAME" "HANDLER-FRAME-P" "MAKE-HANDLER-FRAME"
   "HANDLER-FRAME-CONDITIONS"
   "RESTART-FRAME" "RESTART-FRAME-P" "MAKE-RESTART-FRAME"
   "RESTART-FRAME-RESTART" "RESTART-FRAME-ARGUMENTS")

  (:export
   "MAKE-CLOSURE" "CLOSURE" "CLOSUREP" "CLOSURE-FUNCTION" "CLOSURE-ENVIRONMENT"
   "MAKE-FUNCTION" "FUNCTION" "FUNCTIONP" "FUNCTION-NAME"
   "FUNCTION-NMANDATORY"
   "FUNCTION-NOPTIONAL"  "FUNCTION-IOPTIONAL"
   "FUNCTION-NREST"
   "FUNCTION-NKEY" "FUNCTION-IKEY" "FUNCTION-KKEY"
   "FUNCTION-NAUX" "FUNCTION-IAUX"
   "FUNCTION-LAMBDA-LIST" "FUNCTION-LAMBDA-EXPRESSION"
   "VARIABLE-VALUE" "SET-VARIABLE-VALUE"
   "SYMBOL-MAKE-UNBOUND" "SYMBOL-BOUNDP" "SYMBOL-VALUE" "SET-SYMBOL-VALUE"
   "SYMBOL-MAKE-FUNBOUND" "SYMBOL-FBOUNDP" "SYMBOL-FUNCTION" "SET-SYMBOL-FUNCTION"
   "EVAL" "APPLY" "FUNCALL"
   "CALL-FUNCTION" "EVALE"))

(cl:in-package "COM.INFORMATIMAGO.BOCL.KERNEL")

#define _POSIX_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <ctype.h>
#include "kernel.h"
#include "kernel_types.h"
#include "kernel_objects.h"
#include "kernel_private.h"
#include "macros.h"
#include "memory.h"
#include "boerror.h"
#include "cons.h"
#include "data.h"
#include "structure.h"
#include "variable.h"
#include "stream.h"
#include "reader.h"
#include "printer.h"
#include "structure_functions.h"

/* Internal Object Structure */

static Ref                       Object_new                         (Ref  class,        Slots*   slots);

static Array*                    Array_slots                        (Ref  element_type, halfword rank, halfword* dimensions);
static BuiltInClass*             BuiltInClass_slots                 (Ref  name,         Ref  superclasses);
static Character*                Character_slots                    (Ref  name,         Ref  code);
static Class*                    Class_slots                        (Ref  name,         Ref  superclasses);
static Cons*                     Cons_slots                         (Ref  car,          Ref  cdr);
static Float*                    Float_slots                        (floating value);
static Closure*                  Closure_slots                      (Ref  function,     Ref environment);
static Function*                 Function_slots                     (Ref  lisp_name,    Ref lambda_list, Ref body);
static FuncallableStandardClass* FuncallableStandardClass_slots     (Ref  name ,        Ref superclasses) unused;
static Integer*                  Integer_slots                      (word value);
static OctetArray*               OctetArray_slots                   (Ref  element_type, halfword rank, halfword* dimensions);
static OctetArray*               OctetArray_slots_cstring           (Ref  element_type, const char* initial_value);
static OctetArray*               OctetArray_slots_vector_of_octets  (Ref  element_type, VectorOfOctet* octets);
static StandardClass*            StandardClass_slots                (Ref  name,         Ref  superclasses) unused;
static StructureClass*           StructureClass_slots               (Ref  class_name,   Ref  direct_superclass, Ref offset, Ref slot_names);
static Symbol*                   Symbol_slots                       (Ref  name,         Ref  package);
static VectorOfObject*           VectorOfObject_slots               (halfword size);
static VectorOfOctet*            VectorOfOctet_slots                (halfword length);
static VectorOfOctet*            VectorOfOctet_slots_cstring        (const char* value);
static VectorOfUWord*            VectorOfUWord_slots                (halfword size) unused;
static Environment*              Environment_slots                  (Ref next_environment,bool is_toplevel);




/****************************************/
/*** Object and Slots *******************/
/****************************************/

/* Object */

Ref Object_new(Ref class,Slots* slots){
    Ref that=allocate(sizeof(*that));
    that->objcount=2;
    that->class=class;
    that->slots=slots;
    return that;}

Ref make_boot_Object(Slots* slots){
    Ref that=allocate(sizeof(*that));
    that->objcount=2;
    that->class=&uninitialized_object;
    that->slots=slots;
    return that;}

/* VectorOfObject */

VectorOfObject* VectorOfObject_slots(halfword size){
    VectorOfObject* that=allocate(offsetof(VectorOfObject,elements)+sizeof(that->elements[0])*VARRAY_MIN(size));
    that->objcount=size;
    for(halfword i=0;i<size;i++){
        that->elements[i]=NIL_Symbol;}
    return that;}

/* VectorOfUWord */

VectorOfUWord* VectorOfUWord_slots(halfword size){
    VectorOfUWord* that=allocate(offsetof(VectorOfUWord,elements)+sizeof(that->elements[0])*VARRAY_MIN(size));
    memset(that->elements,CHECK_HALFWORD_TO_INT(size),sizeof(that->elements[0]));
    return that;}


/* VectorOfOctet */

VectorOfOctet* VectorOfOctet_slots(halfword length){
    VectorOfOctet* that=allocate(offsetof(VectorOfOctet,elements)+sizeof(that->elements[0])*VARRAY_MIN(length));
    that->length=length;
    memset(that->elements,CHECK_HALFWORD_TO_INT(length),sizeof(that->elements[0]));
    return that;}

VectorOfOctet* VectorOfOctet_slots_cstring(const char* value){
    size_t len=strlen(value);
    if(HALFWORD_MAX<len){
        ERROR("string too long %ld",len);}
    halfword length=(halfword)len;
    VectorOfOctet* that=allocate(offsetof(VectorOfOctet,elements)+sizeof(that->elements[0])*VARRAY_MIN(length));
    that->length=length;
    for(halfword i=0;i<length;i++){
        that->elements[i]=(octet)value[i];}
    return that;}

/* ARRAY */
/* Used both for ARRAY and VECTOR classes, */
/* as long as element-type is not octet, bit or character. */
Array* Array_slots(Ref element_type,halfword rank,halfword* dimensions)  {
    halfword total_array_size=1;
    for(halfword i=0;i<rank;i++){
        total_array_size=(halfword)(total_array_size*dimensions[i]);}
    VectorOfObject* elements=VectorOfObject_slots(total_array_size);
    Array* that=allocate(offsetof(Array,dimensions)+sizeof(that->dimensions[0])*VARRAY_MIN(rank));
    that->objcount=2;
    that->element_type=element_type;
    that->elements=elements;
    for(halfword i=0;i<rank;i++){
        that->dimensions[i]=dimensions[i];}
    return that;}


/* OCTET-ARRAY */
/* Used both for ARRAY and VECTOR classes when */
/* element-type is octet, bit or character. */

OctetArray* OctetArray_slots(Ref element_type,halfword rank,halfword* dimensions){
    halfword total_array_size=1;
    for(halfword i=0;i<rank;i++){
        total_array_size=(halfword)(total_array_size*dimensions[i]);}
    VectorOfOctet* elements=VectorOfOctet_slots(total_array_size);
    OctetArray* that=allocate(offsetof(OctetArray,dimensions)+sizeof(that->dimensions[0])*VARRAY_MIN(rank));
    that->objcount=2;
    that->element_type=element_type;
    that->elements=elements;
    for(halfword i=0;i<rank;i++){
        that->dimensions[i]=dimensions[i];}
    return that;}

OctetArray* OctetArray_slots_cstring(Ref element_type,const char* initial_value){
    VectorOfOctet* elements=VectorOfOctet_slots_cstring(initial_value);
    halfword rank=1;
    OctetArray* that=allocate(offsetof(OctetArray,dimensions)+sizeof(that->dimensions[0])*VARRAY_MIN(rank));
    that->objcount=2;
    that->element_type=element_type;
    that->elements=elements;
    that->dimensions[0]=elements->length;
    return that;}

static OctetArray* OctetArray_slots_vector_of_octets(Ref element_type, VectorOfOctet* octets){
    halfword rank=1;
    OctetArray* that=allocate(offsetof(OctetArray,dimensions)+sizeof(that->dimensions[0])*VARRAY_MIN(rank));
    that->objcount=2;
    that->element_type=element_type;
    that->elements=octets;
    that->dimensions[0]=octets->length;
    return that;}


/* CHARACTER */

static Character* Character_slots(Ref name,Ref code){
    Character* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=name;
    that->code=code;
    return that;}

static Ref Character_new(word code,const char* name){
    return Object_new(CHARACTER_Class,
                      (Slots*)Character_slots(string_new_cstring(name),
                                              integer_from_word(code)));}

bool characterp(Ref object){
    return class_of(object)==CHARACTER_Class;}

Ref character_code(Ref object){
    check_class(object,CHARACTER_Class);
    return object->slots->character.code;}

Ref character_name(Ref object){
    check_class(object,CHARACTER_Class);
    return object->slots->character.name;}


static Ref character_table[CHAR_CODE_LIMIT];
static Ref characterset;

Ref code_character(Ref code){
    check_class(code,INTEGER_Class);
    word icode=integer_value(code);
    if((icode<0)||(CHAR_CODE_LIMIT<=icode)){
        return NIL_Symbol;}
    else{
        return character_table[icode];}}

Ref octet_character(octet code){
    return character_table[code];}

Ref name_character(Ref name){
    if(characterp(name)){
        return name;}
    else if(symbolp(name)){
        return name_character(symbol_name(name));}
    else if(stringp(name)){
        if(string_length(name)==1){
            return octet_character(name->slots->octet_array.elements->elements[0]);}
        for(int i=0;i<CHAR_CODE_LIMIT;i++){
            if(string_equal(name,character_name(character_table[i]))){
                return character_table[i];}}
        return NIL_Symbol;}
    else{
        /* TODO: STRING-DESIGNATOR type! */
        TYPE_ERROR(name,STRING_Class);}}

static void kernel_initialize_characters(void){

    character_table[  0] = Character_new(0x00, "NULL");
    character_table[  1] = Character_new(0x01, "SOH");
    character_table[  2] = Character_new(0x02, "STX");
    character_table[  3] = Character_new(0x03, "ETX");
    character_table[  4] = Character_new(0x04, "EOT");
    character_table[  5] = Character_new(0x05, "ENQ");
    character_table[  6] = Character_new(0x06, "ACK");
    character_table[  7] = Character_new(0x07, "BELL");
    character_table[  8] = Character_new(0x08, "Backspace");
    character_table[  9] = Character_new(0x09, "Tab");
    character_table[ 10] = Character_new(0x0A, "Linefeed");
    character_table[ 11] = Character_new(0x0B, "VT");
    character_table[ 12] = Character_new(0x0C, "Page");
    character_table[ 13] = Character_new(0x0D, "Return");
    character_table[ 14] = Character_new(0x0E, "SO");
    character_table[ 15] = Character_new(0x0F, "SI");
    character_table[ 16] = Character_new(0x10, "DLE");
    character_table[ 17] = Character_new(0x11, "DC1");
    character_table[ 18] = Character_new(0x12, "DC2");
    character_table[ 19] = Character_new(0x13, "DC3");
    character_table[ 20] = Character_new(0x14, "DC4");
    character_table[ 21] = Character_new(0x15, "NAK");
    character_table[ 22] = Character_new(0x16, "SYN");
    character_table[ 23] = Character_new(0x17, "ETB");
    character_table[ 24] = Character_new(0x18, "CAN");
    character_table[ 25] = Character_new(0x19, "EM");
    character_table[ 26] = Character_new(0x1A, "SUB");
    character_table[ 27] = Character_new(0x1B, "ESCAPE");
    character_table[ 28] = Character_new(0x1C, "FS");
    character_table[ 29] = Character_new(0x1D, "GS");
    character_table[ 30] = Character_new(0x1E, "RS");
    character_table[ 31] = Character_new(0x1F, "US");
    character_table[ 32] = Character_new(0x20, "Space");
    character_table[ 33] = Character_new(0x21, "EXCLAMATION_MARK");
    character_table[ 34] = Character_new(0x22, "QUOTATION_MARK");
    character_table[ 35] = Character_new(0x23, "NUMBER_SIGN");
    character_table[ 36] = Character_new(0x24, "DOLLAR_SIGN");
    character_table[ 37] = Character_new(0x25, "PERCENT_SIGN");
    character_table[ 38] = Character_new(0x26, "AMPERSAND");
    character_table[ 39] = Character_new(0x27, "APOSTROPHE");
    character_table[ 40] = Character_new(0x28, "LEFT_PARENTHESIS");
    character_table[ 41] = Character_new(0x29, "RIGHT_PARENTHESIS");
    character_table[ 42] = Character_new(0x2A, "ASTERISK");
    character_table[ 43] = Character_new(0x2B, "PLUS_SIGN");
    character_table[ 44] = Character_new(0x2C, "COMMA");
    character_table[ 45] = Character_new(0x2D, "HYPHEN-MINUS");
    character_table[ 46] = Character_new(0x2E, "FULL_STOP");
    character_table[ 47] = Character_new(0x2F, "SOLIDUS");
    character_table[ 48] = Character_new(0x30, "DIGIT_ZERO");
    character_table[ 49] = Character_new(0x31, "DIGIT_ONE");
    character_table[ 50] = Character_new(0x32, "DIGIT_TWO");
    character_table[ 51] = Character_new(0x33, "DIGIT_THREE");
    character_table[ 52] = Character_new(0x34, "DIGIT_FOUR");
    character_table[ 53] = Character_new(0x35, "DIGIT_FIVE");
    character_table[ 54] = Character_new(0x36, "DIGIT_SIX");
    character_table[ 55] = Character_new(0x37, "DIGIT_SEVEN");
    character_table[ 56] = Character_new(0x38, "DIGIT_EIGHT");
    character_table[ 57] = Character_new(0x39, "DIGIT_NINE");
    character_table[ 58] = Character_new(0x3A, "COLON");
    character_table[ 59] = Character_new(0x3B, "SEMICOLON");
    character_table[ 60] = Character_new(0x3C, "LESS-THAN_SIGN");
    character_table[ 61] = Character_new(0x3D, "EQUALS_SIGN");
    character_table[ 62] = Character_new(0x3E, "GREATER-THAN_SIGN");
    character_table[ 63] = Character_new(0x3F, "QUESTION_MARK");
    character_table[ 64] = Character_new(0x40, "COMMERCIAL_AT");
    character_table[ 65] = Character_new(0x41, "LATIN_CAPITAL_LETTER_A");
    character_table[ 66] = Character_new(0x42, "LATIN_CAPITAL_LETTER_B");
    character_table[ 67] = Character_new(0x43, "LATIN_CAPITAL_LETTER_C");
    character_table[ 68] = Character_new(0x44, "LATIN_CAPITAL_LETTER_D");
    character_table[ 69] = Character_new(0x45, "LATIN_CAPITAL_LETTER_E");
    character_table[ 70] = Character_new(0x46, "LATIN_CAPITAL_LETTER_F");
    character_table[ 71] = Character_new(0x47, "LATIN_CAPITAL_LETTER_G");
    character_table[ 72] = Character_new(0x48, "LATIN_CAPITAL_LETTER_H");
    character_table[ 73] = Character_new(0x49, "LATIN_CAPITAL_LETTER_I");
    character_table[ 74] = Character_new(0x4A, "LATIN_CAPITAL_LETTER_J");
    character_table[ 75] = Character_new(0x4B, "LATIN_CAPITAL_LETTER_K");
    character_table[ 76] = Character_new(0x4C, "LATIN_CAPITAL_LETTER_L");
    character_table[ 77] = Character_new(0x4D, "LATIN_CAPITAL_LETTER_M");
    character_table[ 78] = Character_new(0x4E, "LATIN_CAPITAL_LETTER_N");
    character_table[ 79] = Character_new(0x4F, "LATIN_CAPITAL_LETTER_O");
    character_table[ 80] = Character_new(0x50, "LATIN_CAPITAL_LETTER_P");
    character_table[ 81] = Character_new(0x51, "LATIN_CAPITAL_LETTER_Q");
    character_table[ 82] = Character_new(0x52, "LATIN_CAPITAL_LETTER_R");
    character_table[ 83] = Character_new(0x53, "LATIN_CAPITAL_LETTER_S");
    character_table[ 84] = Character_new(0x54, "LATIN_CAPITAL_LETTER_T");
    character_table[ 85] = Character_new(0x55, "LATIN_CAPITAL_LETTER_U");
    character_table[ 86] = Character_new(0x56, "LATIN_CAPITAL_LETTER_V");
    character_table[ 87] = Character_new(0x57, "LATIN_CAPITAL_LETTER_W");
    character_table[ 88] = Character_new(0x58, "LATIN_CAPITAL_LETTER_X");
    character_table[ 89] = Character_new(0x59, "LATIN_CAPITAL_LETTER_Y");
    character_table[ 90] = Character_new(0x5A, "LATIN_CAPITAL_LETTER_Z");
    character_table[ 91] = Character_new(0x5B, "LEFT_SQUARE_BRACKET");
    character_table[ 92] = Character_new(0x5C, "REVERSE_SOLIDUS");
    character_table[ 93] = Character_new(0x5D, "RIGHT_SQUARE_BRACKET");
    character_table[ 94] = Character_new(0x5E, "CIRCUMFLEX_ACCENT");
    character_table[ 95] = Character_new(0x5F, "LOW_LINE");
    character_table[ 96] = Character_new(0x60, "GRAVE_ACCENT");
    character_table[ 97] = Character_new(0x61, "LATIN_SMALL_LETTER_A");
    character_table[ 98] = Character_new(0x62, "LATIN_SMALL_LETTER_B");
    character_table[ 99] = Character_new(0x63, "LATIN_SMALL_LETTER_C");
    character_table[100] = Character_new(0x64, "LATIN_SMALL_LETTER_D");
    character_table[101] = Character_new(0x65, "LATIN_SMALL_LETTER_E");
    character_table[102] = Character_new(0x66, "LATIN_SMALL_LETTER_F");
    character_table[103] = Character_new(0x67, "LATIN_SMALL_LETTER_G");
    character_table[104] = Character_new(0x68, "LATIN_SMALL_LETTER_H");
    character_table[105] = Character_new(0x69, "LATIN_SMALL_LETTER_I");
    character_table[106] = Character_new(0x6A, "LATIN_SMALL_LETTER_J");
    character_table[107] = Character_new(0x6B, "LATIN_SMALL_LETTER_K");
    character_table[108] = Character_new(0x6C, "LATIN_SMALL_LETTER_L");
    character_table[109] = Character_new(0x6D, "LATIN_SMALL_LETTER_M");
    character_table[110] = Character_new(0x6E, "LATIN_SMALL_LETTER_N");
    character_table[111] = Character_new(0x6F, "LATIN_SMALL_LETTER_O");
    character_table[112] = Character_new(0x70, "LATIN_SMALL_LETTER_P");
    character_table[113] = Character_new(0x71, "LATIN_SMALL_LETTER_Q");
    character_table[114] = Character_new(0x72, "LATIN_SMALL_LETTER_R");
    character_table[115] = Character_new(0x73, "LATIN_SMALL_LETTER_S");
    character_table[116] = Character_new(0x74, "LATIN_SMALL_LETTER_T");
    character_table[117] = Character_new(0x75, "LATIN_SMALL_LETTER_U");
    character_table[118] = Character_new(0x76, "LATIN_SMALL_LETTER_V");
    character_table[119] = Character_new(0x77, "LATIN_SMALL_LETTER_W");
    character_table[120] = Character_new(0x78, "LATIN_SMALL_LETTER_X");
    character_table[121] = Character_new(0x79, "LATIN_SMALL_LETTER_Y");
    character_table[122] = Character_new(0x7A, "LATIN_SMALL_LETTER_Z");
    character_table[123] = Character_new(0x7B, "LEFT_CURLY_BRACKET");
    character_table[124] = Character_new(0x7C, "VERTICAL_LINE");
    character_table[125] = Character_new(0x7D, "RIGHT_CURLY_BRACKET");
    character_table[126] = Character_new(0x7E, "TILDE");
    character_table[127] = Character_new(0x7F, "Rubout");
    character_table[128] = Character_new(0x80, "PAD");
    character_table[129] = Character_new(0x81, "HOP");
    character_table[130] = Character_new(0x82, "BPH");
    character_table[131] = Character_new(0x83, "NBH");
    character_table[132] = Character_new(0x84, "IND");
    character_table[133] = Character_new(0x85, "Newline");
    character_table[134] = Character_new(0x86, "SSA");
    character_table[135] = Character_new(0x87, "ESA");
    character_table[136] = Character_new(0x88, "HTS");
    character_table[137] = Character_new(0x89, "HTJ");
    character_table[138] = Character_new(0x8A, "VTS");
    character_table[139] = Character_new(0x8B, "PLD");
    character_table[140] = Character_new(0x8C, "PLU");
    character_table[141] = Character_new(0x8D, "RI");
    character_table[142] = Character_new(0x8E, "SS2");
    character_table[143] = Character_new(0x8F, "SS3");
    character_table[144] = Character_new(0x90, "DCS");
    character_table[145] = Character_new(0x91, "PU1");
    character_table[146] = Character_new(0x92, "PU2");
    character_table[147] = Character_new(0x93, "STS");
    character_table[148] = Character_new(0x94, "CCH");
    character_table[149] = Character_new(0x95, "MW");
    character_table[150] = Character_new(0x96, "SPA");
    character_table[151] = Character_new(0x97, "EPA");
    character_table[152] = Character_new(0x98, "SOS");
    character_table[153] = Character_new(0x99, "SGCI");
    character_table[154] = Character_new(0x9A, "SCI");
    character_table[155] = Character_new(0x9B, "CSI");
    character_table[156] = Character_new(0x9C, "ST");
    character_table[157] = Character_new(0x9D, "OSC");
    character_table[158] = Character_new(0x9E, "PM");
    character_table[159] = Character_new(0x9F, "APC");
    character_table[160] = Character_new(0xA0, "NO-BREAK_SPACE");
    character_table[161] = Character_new(0xA1, "INVERTED_EXCLAMATION_MARK");
    character_table[162] = Character_new(0xA2, "CENT_SIGN");
    character_table[163] = Character_new(0xA3, "POUND_SIGN");
    character_table[164] = Character_new(0xA4, "CURRENCY_SIGN");
    character_table[165] = Character_new(0xA5, "YEN_SIGN");
    character_table[166] = Character_new(0xA6, "BROKEN_BAR");
    character_table[167] = Character_new(0xA7, "SECTION_SIGN");
    character_table[168] = Character_new(0xA8, "DIAERESIS");
    character_table[169] = Character_new(0xA9, "COPYRIGHT_SIGN");
    character_table[170] = Character_new(0xAA, "FEMININE_ORDINAL_INDICATOR");
    character_table[171] = Character_new(0xAB, "LEFT-POINTING_DOUBLE_ANGLE_QUOTATION_MARK");
    character_table[172] = Character_new(0xAC, "NOT_SIGN");
    character_table[173] = Character_new(0xAD, "SOFT_HYPHEN");
    character_table[174] = Character_new(0xAE, "REGISTERED_SIGN");
    character_table[175] = Character_new(0xAF, "MACRON");
    character_table[176] = Character_new(0xB0, "DEGREE_SIGN");
    character_table[177] = Character_new(0xB1, "PLUS-MINUS_SIGN");
    character_table[178] = Character_new(0xB2, "SUPERSCRIPT_TWO");
    character_table[179] = Character_new(0xB3, "SUPERSCRIPT_THREE");
    character_table[180] = Character_new(0xB4, "ACUTE_ACCENT");
    character_table[181] = Character_new(0xB5, "MICRO_SIGN");
    character_table[182] = Character_new(0xB6, "PILCROW_SIGN");
    character_table[183] = Character_new(0xB7, "MIDDLE_DOT");
    character_table[184] = Character_new(0xB8, "CEDILLA");
    character_table[185] = Character_new(0xB9, "SUPERSCRIPT_ONE");
    character_table[186] = Character_new(0xBA, "MASCULINE_ORDINAL_INDICATOR");
    character_table[187] = Character_new(0xBB, "RIGHT-POINTING_DOUBLE_ANGLE_QUOTATION_MARK");
    character_table[188] = Character_new(0xBC, "VULGAR_FRACTION_ONE_QUARTER");
    character_table[189] = Character_new(0xBD, "VULGAR_FRACTION_ONE_HALF");
    character_table[190] = Character_new(0xBE, "VULGAR_FRACTION_THREE_QUARTERS");
    character_table[191] = Character_new(0xBF, "INVERTED_QUESTION_MARK");
    character_table[192] = Character_new(0xC0, "LATIN_CAPITAL_LETTER_A_WITH_GRAVE");
    character_table[193] = Character_new(0xC1, "LATIN_CAPITAL_LETTER_A_WITH_ACUTE");
    character_table[194] = Character_new(0xC2, "LATIN_CAPITAL_LETTER_A_WITH_CIRCUMFLEX");
    character_table[195] = Character_new(0xC3, "LATIN_CAPITAL_LETTER_A_WITH_TILDE");
    character_table[196] = Character_new(0xC4, "LATIN_CAPITAL_LETTER_A_WITH_DIAERESIS");
    character_table[197] = Character_new(0xC5, "LATIN_CAPITAL_LETTER_A_WITH_RING_ABOVE");
    character_table[198] = Character_new(0xC6, "LATIN_CAPITAL_LETTER_AE");
    character_table[199] = Character_new(0xC7, "LATIN_CAPITAL_LETTER_C_WITH_CEDILLA");
    character_table[200] = Character_new(0xC8, "LATIN_CAPITAL_LETTER_E_WITH_GRAVE");
    character_table[201] = Character_new(0xC9, "LATIN_CAPITAL_LETTER_E_WITH_ACUTE");
    character_table[202] = Character_new(0xCA, "LATIN_CAPITAL_LETTER_E_WITH_CIRCUMFLEX");
    character_table[203] = Character_new(0xCB, "LATIN_CAPITAL_LETTER_E_WITH_DIAERESIS");
    character_table[204] = Character_new(0xCC, "LATIN_CAPITAL_LETTER_I_WITH_GRAVE");
    character_table[205] = Character_new(0xCD, "LATIN_CAPITAL_LETTER_I_WITH_ACUTE");
    character_table[206] = Character_new(0xCE, "LATIN_CAPITAL_LETTER_I_WITH_CIRCUMFLEX");
    character_table[207] = Character_new(0xCF, "LATIN_CAPITAL_LETTER_I_WITH_DIAERESIS");
    character_table[208] = Character_new(0xD0, "LATIN_CAPITAL_LETTER_ETH");
    character_table[209] = Character_new(0xD1, "LATIN_CAPITAL_LETTER_N_WITH_TILDE");
    character_table[210] = Character_new(0xD2, "LATIN_CAPITAL_LETTER_O_WITH_GRAVE");
    character_table[211] = Character_new(0xD3, "LATIN_CAPITAL_LETTER_O_WITH_ACUTE");
    character_table[212] = Character_new(0xD4, "LATIN_CAPITAL_LETTER_O_WITH_CIRCUMFLEX");
    character_table[213] = Character_new(0xD5, "LATIN_CAPITAL_LETTER_O_WITH_TILDE");
    character_table[214] = Character_new(0xD6, "LATIN_CAPITAL_LETTER_O_WITH_DIAERESIS");
    character_table[215] = Character_new(0xD7, "MULTIPLICATION_SIGN");
    character_table[216] = Character_new(0xD8, "LATIN_CAPITAL_LETTER_O_WITH_STROKE");
    character_table[217] = Character_new(0xD9, "LATIN_CAPITAL_LETTER_U_WITH_GRAVE");
    character_table[218] = Character_new(0xDA, "LATIN_CAPITAL_LETTER_U_WITH_ACUTE");
    character_table[219] = Character_new(0xDB, "LATIN_CAPITAL_LETTER_U_WITH_CIRCUMFLEX");
    character_table[220] = Character_new(0xDC, "LATIN_CAPITAL_LETTER_U_WITH_DIAERESIS");
    character_table[221] = Character_new(0xDD, "LATIN_CAPITAL_LETTER_Y_WITH_ACUTE");
    character_table[222] = Character_new(0xDE, "LATIN_CAPITAL_LETTER_THORN");
    character_table[223] = Character_new(0xDF, "LATIN_SMALL_LETTER_SHARP_S");
    character_table[224] = Character_new(0xE0, "LATIN_SMALL_LETTER_A_WITH_GRAVE");
    character_table[225] = Character_new(0xE1, "LATIN_SMALL_LETTER_A_WITH_ACUTE");
    character_table[226] = Character_new(0xE2, "LATIN_SMALL_LETTER_A_WITH_CIRCUMFLEX");
    character_table[227] = Character_new(0xE3, "LATIN_SMALL_LETTER_A_WITH_TILDE");
    character_table[228] = Character_new(0xE4, "LATIN_SMALL_LETTER_A_WITH_DIAERESIS");
    character_table[229] = Character_new(0xE5, "LATIN_SMALL_LETTER_A_WITH_RING_ABOVE");
    character_table[230] = Character_new(0xE6, "LATIN_SMALL_LETTER_AE");
    character_table[231] = Character_new(0xE7, "LATIN_SMALL_LETTER_C_WITH_CEDILLA");
    character_table[232] = Character_new(0xE8, "LATIN_SMALL_LETTER_E_WITH_GRAVE");
    character_table[233] = Character_new(0xE9, "LATIN_SMALL_LETTER_E_WITH_ACUTE");
    character_table[234] = Character_new(0xEA, "LATIN_SMALL_LETTER_E_WITH_CIRCUMFLEX");
    character_table[235] = Character_new(0xEB, "LATIN_SMALL_LETTER_E_WITH_DIAERESIS");
    character_table[236] = Character_new(0xEC, "LATIN_SMALL_LETTER_I_WITH_GRAVE");
    character_table[237] = Character_new(0xED, "LATIN_SMALL_LETTER_I_WITH_ACUTE");
    character_table[238] = Character_new(0xEE, "LATIN_SMALL_LETTER_I_WITH_CIRCUMFLEX");
    character_table[239] = Character_new(0xEF, "LATIN_SMALL_LETTER_I_WITH_DIAERESIS");
    character_table[240] = Character_new(0xF0, "LATIN_SMALL_LETTER_ETH");
    character_table[241] = Character_new(0xF1, "LATIN_SMALL_LETTER_N_WITH_TILDE");
    character_table[242] = Character_new(0xF2, "LATIN_SMALL_LETTER_O_WITH_GRAVE");
    character_table[243] = Character_new(0xF3, "LATIN_SMALL_LETTER_O_WITH_ACUTE");
    character_table[244] = Character_new(0xF4, "LATIN_SMALL_LETTER_O_WITH_CIRCUMFLEX");
    character_table[245] = Character_new(0xF5, "LATIN_SMALL_LETTER_O_WITH_TILDE");
    character_table[246] = Character_new(0xF6, "LATIN_SMALL_LETTER_O_WITH_DIAERESIS");
    character_table[247] = Character_new(0xF7, "DIVISION_SIGN");
    character_table[248] = Character_new(0xF8, "LATIN_SMALL_LETTER_O_WITH_STROKE");
    character_table[249] = Character_new(0xF9, "LATIN_SMALL_LETTER_U_WITH_GRAVE");
    character_table[250] = Character_new(0xFA, "LATIN_SMALL_LETTER_U_WITH_ACUTE");
    character_table[251] = Character_new(0xFB, "LATIN_SMALL_LETTER_U_WITH_CIRCUMFLEX");
    character_table[252] = Character_new(0xFC, "LATIN_SMALL_LETTER_U_WITH_DIAERESIS");
    character_table[253] = Character_new(0xFD, "LATIN_SMALL_LETTER_Y_WITH_ACUTE");
    character_table[254] = Character_new(0xFE, "LATIN_SMALL_LETTER_THORN");
    character_table[255] = Character_new(0xFF, "LATIN_SMALL_LETTER_Y_WITH_DIAERESIS");

    characterset=NIL_Symbol;
    for(int i=0;i<CHAR_CODE_LIMIT;i++){
        push(character_table[i],&characterset);}}


/* INTEGER */
/* Before we use a bigint library, let's just use 64-bit integers: */

static Ref integer_table[INTEGER_INTERN_MAX - INTEGER_INTERN_MIN];
static Ref integerset;

Integer* Integer_slots(word value){
    Integer* that=allocate(sizeof(*that));
    that->value=value;
    return that;}

Ref integer_from_word(word value){
    if((INTEGER_INTERN_MIN<=value) && (value<INTEGER_INTERN_MAX)){
        return integer_table[value-INTEGER_INTERN_MIN];}
    return Object_new(INTEGER_Class,(Slots*)Integer_slots(value));}

Ref integer_from_uword(uword value){
    if(value<INTEGER_INTERN_MAX){
        return integer_table[(word)value-INTEGER_INTERN_MIN];}
    return Object_new(INTEGER_Class,(Slots*)Integer_slots(CHECK_UWORD_TO_WORD(value)));}

bool integerp(Ref object){
    return class_of(object)==INTEGER_Class;}

word integer_value(Ref object){
    check_class(object,INTEGER_Class);
    return object->slots->integer.value;}

static void kernel_initialize_integers(void){
    integerset=NIL_Symbol;
    for(word i=INTEGER_INTERN_MIN;i<INTEGER_INTERN_MAX;i++){
        integer_table[i-INTEGER_INTERN_MIN] = Object_new(INTEGER_Class,(Slots*)Integer_slots(i));
        push(integer_table[i-INTEGER_INTERN_MIN],&integerset);}}


/* FLOAT */

Float* Float_slots(floating value){
    Float* that=allocate(sizeof(*that));
    that->value=value;
    return that;}

Ref float_from_floating(floating value){
    return Object_new(FLOAT_Class,(Slots*)Float_slots(value));}

bool floatp(Ref object){
    return class_of(object)==FLOAT_Class;}

floating float_value(Ref object){
    check_class(object,FLOAT_Class);
    return object->slots->floater.value;}


/* CONS */

Cons* Cons_slots(Ref car,Ref cdr){
    Cons* that=allocate(sizeof(*that));
    that->objcount=2;
    that->car=car;
    that->cdr=cdr;
    return that;}

Ref cons(Ref car,Ref cdr){
    return Object_new(CONS_Class,(Slots*)Cons_slots(car,cdr));}

bool consp(Ref object){
    return class_of(object)==CONS_Class;}

Ref cons_car(Ref cell){
    return cell->slots->cons.car;}

Ref cons_cdr(Ref cell){
    return cell->slots->cons.cdr;}

Ref cons_rplaca(Ref cell,Ref newcar){
    cell->slots->cons.car=newcar;
    return cell;}

Ref cons_rplacd(Ref cell,Ref newcdr){
    cell->slots->cons.cdr=newcdr;
    return cell;}



/* STRING */

Ref string_new(halfword length){
    return Object_new(STRING_Class,
                      (Slots*)OctetArray_slots(CHARACTER_Symbol,1,&length));}

Ref string_new_vector_of_octets(VectorOfOctet* octets){
    return Object_new(STRING_Class,
                      (Slots*)OctetArray_slots_vector_of_octets(CHARACTER_Symbol,octets));}

Ref string_new_cstring(const char* string){
    return Object_new(STRING_Class,
                      (Slots*)OctetArray_slots_cstring(CHARACTER_Symbol,string));}

bool stringp(Ref object){
    return class_of(object)==STRING_Class;}

uword string_length(Ref string){
    check_class(string,STRING_Class);
    return string->slots->octet_array.dimensions[0];}

octet* string_octets(Ref string){
    check_class(string,STRING_Class);
    return string->slots->octet_array.elements->elements;}

char* string_cstring(Ref string){
    check_class(string,STRING_Class);
    size_t length=string->slots->octet_array.dimensions[0];
    char* result=CHECK_POINTER(malloc(1+length));
    strncpy(result,(const char*)string->slots->octet_array.elements->elements,length);
    result[length]='\0';
    return result;}

bool string_eq(Ref a,Ref b){
    if(a==b){
        return true;}
    uword length=string_length(a);
    if(length!=string_length(b)){
        return false;}
    OctetArray* aa=&(a->slots->octet_array);
    OctetArray* bb=&(b->slots->octet_array);
    for(uword i=0;i<length;i++){
        if(aa->elements->elements[i]!=bb->elements->elements[i]){
            return false;}}
    return true;}


static bool vector_of_octet_equalp(VectorOfOctet* a,VectorOfOctet* b){
    if(a->length!=b->length){
        return false;}
    octet* ap=a->elements;
    octet* bp=b->elements;
    for(uword i=0;i<a->length;i++){
        int al=islower(ap[i])?toupper(ap[i]):ap[i];
        int bl=islower(bp[i])?toupper(bp[i]):bp[i];
        if(al!=bl){
            return false;}}
    return true;}

bool string_equal(Ref a,Ref b){
    if(a==b){
        return true;}
    uword length=string_length(a);
    if(length!=string_length(b)){
        return false;}
    OctetArray* aa=&(a->slots->octet_array);
    OctetArray* bb=&(b->slots->octet_array);
    return vector_of_octet_equalp(aa->elements,bb->elements);}



/* SYMBOL */

Symbol* Symbol_slots(Ref name,Ref package){
    Symbol* that=allocate(sizeof(*that));
    that->objcount=5;
    that->name=name;
    that->package=package;
    that->value=UNBOUND_Object;
    that->function=UNBOUND_Object;
    that->plist=NIL_Symbol;
    return that;}

Ref find_symbol(Ref name,Ref obarray){
#ifdef DEBUG
    {
        static long c=0;
        fprintf(stderr,"find_symbol %3ld: %-40s in %-16s",
                c++,string_cstring(name),
                string_cstring(symbol_name(cons_car(obarray))));
        Ref syms=cons_cdr(obarray);
        long sc=0;
        while(consp(syms)){
            sc++;
            if(string_eq(symbol_name(cons_car(syms)),name)){
                fprintf(stderr,"found %s\n",
                        string_cstring(symbol_name(cons_car(syms))));}
            if(symbol_package(cons_car(syms))!=obarray){
                fprintf(stderr,"wrong package for %s\n",
                        string_cstring(symbol_name(cons_car(syms))));}
            syms=cons_cdr(syms);}
        fprintf(stderr," %3ld symbols\n",sc);}
#endif
    Ref syms=cons_cdr(obarray);
    while(consp(syms) && !string_eq(cons_car(syms)->slots->symbol.name,name)){
        syms=cons_cdr(syms);}
    return consp(syms)
            ? cons_car(syms)
            : NULL;}

Ref symbol_new(Ref name){
    /* make an uninterned symbol */
    return Object_new(SYMBOL_Class, (Slots*)Symbol_slots(name,NIL_Symbol));}

Ref symbol_new_cstring(const char* name){
    /* make an uninterned symbol */
    return symbol_new(string_new_cstring(name));}

static Ref symbol_intern_obarray(Ref name,Ref obarray,bool* internal){
    Ref symbol=find_symbol(name,obarray);
    if(symbol){
        (*internal)=true;
        return symbol;}
    (*internal)=false;
    symbol=Object_new(SYMBOL_Class, (Slots*)Symbol_slots(name,obarray));
    cons_rplacd(obarray,cons(symbol,cons_cdr(obarray)));
    return symbol;}

Ref symbol_intern(Ref name){
    bool internal;
    return symbol_intern_obarray(name,KERNEL_obarray,&internal);}

Ref symbol_intern_cstring(const char* name){
    return symbol_intern(string_new_cstring(name));}

bool symbolp(Ref object){
    return typep(object,SYMBOL_Class);}
    /* return class_of(object)==SYMBOL_Class;} */

Ref symbol_package(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    return symbol->slots->symbol.package;}

Ref symbol_name(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    return symbol->slots->symbol.name;}

bool symbol_boundp(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    return UNBOUND_Object!=symbol->slots->symbol.value;}

bool symbol_fboundp(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    return UNBOUND_Object!=symbol->slots->symbol.function;}

Ref symbol_value(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    return symbol->slots->symbol.value;}

Ref symbol_function(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    return symbol->slots->symbol.function;}

Ref symbol_plist(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    return symbol->slots->symbol.plist;}

Ref symbol_make_unbound(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    symbol->slots->symbol.value=UNBOUND_Object;
    return symbol;}

Ref symbol_make_funbound(Ref symbol){
    check_type(symbol,SYMBOL_Class);
    symbol->slots->symbol.function=UNBOUND_Object;
    return symbol;}

Ref set_symbol_package(Ref symbol,Ref new_package){
    check_type(symbol,SYMBOL_Class);
    return symbol->slots->symbol.package=new_package;}

Ref set_symbol_value(Ref symbol,Ref new_value){
    check_type(symbol,SYMBOL_Class);
    return symbol->slots->symbol.value=new_value;}

Ref set_symbol_function(Ref symbol,Ref new_function){
    check_type(symbol,SYMBOL_Class);
    check_type(new_function,FUNCTION_Class);
    return symbol->slots->symbol.function=new_function;}

Ref set_symbol_plist(Ref symbol,Ref new_plist){
    check_type(symbol,SYMBOL_Class);
    if(consp(new_plist)||nullp(new_plist)){
        return symbol->slots->symbol.plist=new_plist;}
    else{
        TYPE_ERROR(new_plist,LIST_Class);}}

Ref obarray_all_symbols(Ref obarray){
    return copy_list(cdr(obarray));}

/* KEYWORD */
/* Note: KEYWORD is not a class, but a subtype of SYMBOL */

Ref keyword_intern(Ref name){
    bool internal;
    Ref keyword=symbol_intern_obarray(name,KEYWORD_obarray,&internal);
    if(!internal){
        define_constant(keyword,keyword);}
    return keyword;}

Ref keyword_intern_cstring(const char* name){
    return keyword_intern(string_new_cstring(name));}

bool keywordp(Ref object){
    return symbolp(object)
            && !nullp(memq(object,cdr(KEYWORD_obarray)));}

/* NULL */
/* NULL class has a SYMBOL superclass and has a NIL_Symbol instance. */

bool nullp(Ref object){
    return object==NIL_Symbol;}


/* BOOLEAN */

Ref as_boolean(bool value){
    return (value
            ?T_Symbol
            :NIL_Symbol);}

bool is_true(Ref object){
    return object!=NIL_Symbol;}


/* VECTOR */

Ref vector_new(Ref elements){
    uword length=list_length(elements);
    halfword dimensions[1];
    dimensions[0]=CHECK_UWORD_TO_HALFWORD(length);
    Array* array=Array_slots(T_Symbol,1,dimensions);
    for(int i=0;consp(elements);i++,elements=cdr(elements)){
        array->elements->elements[i]=car(elements);}
    return Object_new(VECTOR_Class,(Slots*)array);}


/* BIT-VECTOR */

Ref bit_vector_new_octets(uword length,octet* bits){
    halfword dimensions[1];
    dimensions[0]=CHECK_UWORD_TO_HALFWORD(length);
    Array* array=Array_slots(BIT_Symbol,1,dimensions);
    memcpy(array->elements->elements,bits,length);
    return Object_new(BIT_VECTOR_Class,(Slots*)array);}

bool bit_vector_equal(Ref a,Ref b){
    if(a==b){
        return true;}
    uword length=bit_vector_length(a);
    if(length!=bit_vector_length(b)){
        return false;}
    OctetArray* aa=&(a->slots->octet_array);
    OctetArray* bb=&(b->slots->octet_array);
    octet* ap=aa->elements->elements;
    octet* bp=bb->elements->elements;
    for(uword i=0;i<length;i++){
        if(ap[i]!=bp[i]){
            return false;}}
    return true;}


/* CLASS */

Class* Class_slots(Ref name,Ref superclasses){
    Class* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=name;
    that->direct_superclasses=superclasses;
    return that;}

Class* make_boot_Class(){
    Class* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=&uninitialized_object;
    that->direct_superclasses=&uninitialized_object;
    return that;}

Ref class_new(Ref name){
    return Object_new(CLASS_Class,(Slots*)Class_slots(name,NIL_Symbol));}

Ref class_of(Ref object){
    return object->class;}

bool classp(Ref object){
    return class_of(object)==CLASS_Class;}

Ref class_name(Ref class){
    check_type(class,CLASS_Class);
    return class->slots->class.name;}

Ref class_direct_superclasses(Ref class){
    check_type(class,CLASS_Class);
    return class->slots->class.direct_superclasses;}

void class_add_superclass(Ref class, Ref superclass){
    check_type(class,CLASS_Class);
    check_type(superclass,CLASS_Class);
    if(class->slots->class.direct_superclasses){
        class->slots->class.direct_superclasses=nconc(class->slots->class.direct_superclasses,
                                                      cons(superclass,NIL_Symbol));}
    else{
        class->slots->class.direct_superclasses=cons(superclass,NIL_Symbol);}}


Ref find_class(Ref name){
    {dolist(class,kernel_classes){
            if(eql(class->slots->class.name,name)){
                return class;}}}
    return NULL;}

bool subclassp(Ref class,Ref superclass){
    if(class==superclass){
        return true;}
    Ref direct_superclasses=class->slots->class.direct_superclasses;
    while(consp(direct_superclasses)){
        Ref direct_superclass=cons_car(direct_superclasses);
        direct_superclasses=cons_cdr(direct_superclasses);
        if(subclassp(direct_superclass,superclass)){
            return true;}}
    return false;}

bool typep(Ref object,Ref class){
    Ref object_class=class_of(object);
#ifdef DEBUG
    fprintf(stderr,"Is object %p of type %p\n",
            (void*)object,
            (void*)class);
    if(object==NULL){
        fprintf(stderr,"NULL object!\n");
        abort();}
    if(class==NULL){
        fprintf(stderr,"NULL class!\n");
        abort();}
    if(object_class==class){
        return true;}
    fprintf(stderr,"Is object of class %p of type %p\n",
            (void*)object_class->slots->class.name->slots->symbol.name,
            (void*)       class->slots->class.name->slots->symbol.name);
    fprintf(stderr,"Is object of class %s of type %s\n",
            string_cstring(object_class->slots->class.name->slots->symbol.name),
            string_cstring(       class->slots->class.name->slots->symbol.name));
#endif
    /* check_type(class,CLASS_Class); */
    return(object_class==class)
            ?true
            :subclassp(object_class,class);}


void check_type(Ref object,Ref class){
    /* checks that the object is an instance of the class, direct or indirect */
    if(!typep(object,class)){
        TYPE_ERROR(object,class);}}


void check_class(Ref object,Ref class){
    /* checks that the object is a direct instance of the class */
    if(class_of(object)!=class){
        TYPE_ERROR(object,class);}}






Ref boot_last(Ref list){
    Ref next;
    if(consp(list)){
        while(consp(next=list->slots->cons.cdr)){
            list=next;}
        return list;}
    else if(nullp(list)){
        return list;}
    else{
        TYPE_ERROR(list,LIST_Class);}}

Ref boot_nconc(Ref list,Ref tail){
    if(nullp(list)){
        return tail;}
    else if(consp(list)){
        Ref last_cell=boot_last(list);
        last_cell->slots->cons.cdr=tail;
        return list;}
    else{
        TYPE_ERROR(list,LIST_Class);}}

void boot_class_add_superclass(Ref class, Ref superclass){
    if(class->slots->class.direct_superclasses){
        class->slots->class.direct_superclasses=boot_nconc(class->slots->class.direct_superclasses,
                                                           cons(superclass,NIL_Symbol));}
    else{
        class->slots->class.direct_superclasses=cons(superclass,NIL_Symbol);}}



/* STANDARD-CLASS */

StandardClass* StandardClass_slots(Ref name,Ref superclasses){
    StandardClass* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=name;
    that->direct_superclasses=superclasses;
    return that;}

StandardClass* make_boot_StandardClass(void){
    StandardClass* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=&uninitialized_object;
    that->direct_superclasses=&uninitialized_object;
    return that;}


/* BUILT-IN-CLASS */

Ref built_in_class_new(Ref name){
    return Object_new(BUILT_IN_CLASS_Class,(Slots*)BuiltInClass_slots(name,NIL_Symbol));}

BuiltInClass* BuiltInClass_slots(Ref name,Ref superclasses){
    BuiltInClass* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=name;
    that->direct_superclasses=superclasses;
    return that;}

BuiltInClass* make_boot_BuiltInClass(void){
    BuiltInClass* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=&uninitialized_object;
    that->direct_superclasses=&uninitialized_object;
    return that;}


/* STRUCTURE-CLASS */


/* TODO: implement initial-offset
For this, we may have to keep an a-list of slot-names with their
indexes, to deal with the holes, and to record the initial-offset in
the structure-class.
*/


StructureClass* make_boot_StructureClass(void){
    StructureClass* that=allocate(sizeof(*that));
    that->objcount=3;
    that->name=&uninitialized_object;
    that->direct_superclasses=&uninitialized_object;
    that->slot_names=&uninitialized_object;
    return that;}

StructureClass* StructureClass_slots(Ref class_name,Ref direct_superclass,Ref offset, Ref slot_names){
    uword pool=open_live_pool();
    add_to_live_pool(class_name);
    add_to_live_pool(direct_superclass);
    add_to_live_pool(offset);
    add_to_live_pool(slot_names);
    StructureClass* that=allocate(sizeof(*that));
    that->objcount=4;
    that->name=class_name;
    that->direct_superclasses=list(direct_superclass,NULL);
    that->offset=offset;
    that->slot_names=slot_names;
    that->nslots=direct_superclass->slots->structure_class.nslots
            +(uword)integer_value(offset)
            +list_length(slot_names);
    close_live_pool(pool);
    return that;}

Ref make_structure_class(Ref name,Ref direct_superclass,Ref offset,Ref slots){
    uword pool=open_live_pool();
    add_to_live_pool(name);
    add_to_live_pool(direct_superclass);
    add_to_live_pool(offset);
    add_to_live_pool(slots);
    /*
    Creates a class that has structure-class metaclass,
    direct_superclass or structure-object as superclass.

    OFFSET is an offset number of slots inserted before the SLOTS.

    SLOTS is a list of symbols naming the additionnal slots to the new
    structure class.

    (An instance will have the concatenation of all the slots lists of all
    the superclasses of its structure-class).
    */
    check_class(name,SYMBOL_Class);
    if(direct_superclass==NIL_Symbol){
        direct_superclass=STRUCTURE_OBJECT_Class;}
    else{
        check_type(direct_superclass,STRUCTURE_CLASS_Class);}
    check_type(slots,LIST_Class);
    /* TODO: check that all elements of slots is a symbol */
    Ref class=Object_new(STRUCTURE_CLASS_Class,
                         (Slots*)StructureClass_slots(name,direct_superclass,offset,slots));
    close_live_pool(pool);
    return class;}

Ref make_structure_instance(Ref structure_class){
    uword pool=open_live_pool();
    add_to_live_pool(structure_class);
    /*
    Creates an instance of the structure_class.
    All slots are initialized to NIL.
    */
    check_class(structure_class,STRUCTURE_CLASS_Class);
    VectorOfObject* slots=VectorOfObject_slots(CHECK_UWORD_TO_HALFWORD(structure_class->slots->structure_class.nslots));
    Ref instance=Object_new(structure_class,(Slots*)slots);
    close_live_pool(pool);
    return instance;}


/* FUNCALLABLE-STANDARD-CLASS */

FuncallableStandardClass* FuncallableStandardClass_slots(Ref name,Ref superclasses){
    FuncallableStandardClass* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=name;
    that->direct_superclasses=superclasses;
    return that;}

FuncallableStandardClass* make_boot_FuncallableStandardClass(void){
    FuncallableStandardClass* that=allocate(sizeof(*that));
    that->objcount=2;
    that->name=&uninitialized_object;
    that->direct_superclasses=&uninitialized_object;
    return that;}




/* PHYSICAL-PATHNAME */

Ref physical_pathname_new(VectorOfOctet* path){
    return Object_new(find_class(symbol_intern_cstring("PHYSICAL-PATHNAME")),
                      (Slots*)path);}

Ref physical_pathname_new_cstring(const char* path){
    return physical_pathname_new(VectorOfOctet_slots_cstring(path));}

Ref physical_pathname(Ref namestring){
    check_class(namestring,STRING_Class);
    return physical_pathname_new(&namestring->slots->vector_of_octet);}

bool physical_pathname_p(Ref object){
    return typep(object,PATHNAME_Class);}

bool physical_pathname_equal(Ref a,Ref b){
    if(!typep(a,PHYSICAL_PATHNAME_Class)){
        return false;}
    if(!typep(b,PHYSICAL_PATHNAME_Class)){
        return false;}
    if(a==b){
        return true;}
    if(a->slots->vector_of_octet.length
       !=b->slots->vector_of_octet.length){
        return false;}
    OctetArray* aa=&(a->slots->octet_array);
    OctetArray* bb=&(b->slots->octet_array);
    return vector_of_octet_equalp(aa->elements,bb->elements);}


/* EXTERNAL-FORMAT */

/*
External-formats can be:

- :DEFAULT in place of (:iso-8859-1 :lf :error)

- a keyword denoting an encoding in (:us-ascii :iso-8859-1);
  the default line-termination is :lf, and
  the default encoding error handling is :error (signal an error).

- a list containing:

  + a keyword denoting an encoding in (:us-ascii :iso-8859-1),

  + a line-termination keyword (:dos :crlf :mac :cr :unix :lf),
    the default if NIL or absent is :LF.

  + an object specifing the handling of encoding errors:

    * a character used as substitution character #\sub is suggested,
      or an integer used as substitution code 26 (SUB) is suggested.
      (code-char code) is used on input encoding error.
      (char-code char) is used on output decoding error.

    * the keyword :error asking for a continuable encoding-error to be
      signaled, with a substitute-code restart. This is the default if absent.

    * the symbol NIL asking to ignore and skip the unencodable
      character.

Encoding or decoding errors can only occur with a code>127 given with
:us-ascii, since we deal only with :us-ascii (7-bit) and :iso-8859-1
(8-bit) and internal coding is iso-8859-1.


Note: kernel:stream-external-format accept only (:crlf :cr :lf) as
line-termination; the mapping is done in the CL layer.

(list (member :US-ASCII :ISO-8859-1)
      (member :crlf :cr :lf)
      (or character (member :error nil)))
*/

#define DEFAULT_ENCODING      K(ISO-8859-1)
#define DEFAULT_ENCODING_ENUM encoding_iso_8859_1

Ref normalize_encoding(Ref encoding_o,encoding_t* encoding){
    if(encoding_o==K(DEFAULT)){
        (*encoding)=DEFAULT_ENCODING_ENUM;
        return DEFAULT_ENCODING;}
    else if(encoding_o==K(ISO-8859-1)){
        (*encoding)=encoding_iso_8859_1;
        return encoding_o;}
    else if(encoding_o==K(US-ASCII)){
        (*encoding)=encoding_us_ascii;
        return encoding_o;}
    else{
        error("Invalid encoding %s",
              string_cstring(prin1_to_string(encoding_o)));}}

Ref normalize_new_line(Ref new_line_o,new_line_t* new_line){
    if((new_line_o==K(LF))||(new_line_o==K(UNIX))
       ||(new_line_o==K(DEFAULT))||(new_line_o==NIL_Symbol)){
        (*new_line)=new_line_lf;
        return K(LF);}
    else if((new_line_o==K(CR))||(new_line_o==K(MAC))){
        (*new_line)=new_line_cr;
        return K(CR);}
    else if((new_line_o==K(CRLF))||(new_line_o==K(DOS))){
        (*new_line)=new_line_crlf;
        return K(CRLF);}
    else{
        error("Invalid new-line format %s",
              string_cstring(prin1_to_string(new_line_o)));}}

Ref normalize_on_error(Ref on_error_o,on_error_t* on_error){
    if(characterp(on_error_o)){
        word value=integer_value(character_code(on_error_o));
        (*on_error)=(on_error_t)value;
        return on_error_o;}
    else if(on_error_o==K(ERROR)){
        (*on_error)=on_error_error;
        return on_error_o;}
    else if(on_error_o==NIL_Symbol){
        (*on_error)=on_error_ignore;
        return on_error_o;}
    else{
        error("Invalid on-error format %s",
              string_cstring(prin1_to_string(on_error_o)));}}

Ref normalize_external_format_internal(Ref external_format,
                                  encoding_t* encoding,
                                  new_line_t* new_line,
                                  on_error_t* on_error){
    Ref encoding_o=DEFAULT_ENCODING;
    Ref new_line_o=K(LF);
    Ref on_error_o=K(ERROR);
    if(symbolp(external_format)){
        encoding_o=normalize_encoding(external_format,encoding);}
    else if(consp(external_format)){
        encoding_o=normalize_encoding(car(external_format),encoding);
        new_line_o=normalize_new_line(cadr(external_format),new_line);
        if(nullp(cddr(external_format))){
            on_error_o=K(ERROR);
            (*on_error)=on_error_error;}
        else{
            on_error_o=normalize_on_error(caddr(external_format),on_error);}}
    else{
        error("Invalid external-format %s",
              string_cstring(prin1_to_string(external_format)));}
    return cons(encoding_o,cons(new_line_o,cons(on_error_o,NIL_Symbol)));}

Ref normalize_external_format(Ref external_format){
    encoding_t encoding;
    new_line_t new_line;
    on_error_t on_error;
    return normalize_external_format_internal(external_format, &encoding,&new_line,&on_error);}


direction_t convert_direction(Ref direction){
    if(direction==K(INPUT)){
        return direction_input;}
    else if(direction==K(OUTPUT)){
        return direction_output;}
    else if(direction==K(IO)){
        return direction_io;}
    else if(direction==K(PROBE)){
        return direction_probe;}
    else {
        ERROR("Invalid direction %s",
              string_cstring(prin1_to_string(direction)));}}

Ref convert_from_direction(direction_t direction){
    switch(direction){
      case direction_input:   return K(INPUT);
      case direction_output:  return K(OUTPUT);
      case direction_io:      return K(IO);
      case direction_probe:   return K(PROBE);
      default:
          FATAL("Invalid direction value %d",direction);}}

type_t convert_element_type(Ref element_type){
    /* CHARACTER or (UNSIGNED-BYTE 8) or BIT */
    if(element_type==S(CHARACTER)){
        return type_character;}
    if(element_type==S(BIT)){
        return type_bit;}
    if((consp(element_type)
        &&(car(element_type)==S(UNSIGNED-BYTE))
        &&(consp(cdr(element_type)))
        &&(integerp(cadr(element_type)))
        &&(integer_value(cadr(element_type))==8)
        &&(nullp(cddr(element_type))))){
        return type_octet;}
    ERROR("Invalid element-type %s",
          string_cstring(prin1_to_string(element_type)));}



/* FILE-STREAM */

Ref file_stream_new(Ref pathname,     /* a PHYSICAL-PATHNAME */
                    Ref element_type, /* CHARACTER or (UNSIGNED-BYTE 8) or BIT */
                    Ref external_format){
    check_class(pathname,PHYSICAL_PATHNAME_Class);
    type_t type=convert_element_type(element_type);
    encoding_t encoding=encoding_iso_8859_1;
    new_line_t new_line=new_line_lf;
    on_error_t on_error=on_error_error;
    external_format=normalize_external_format_internal(external_format,&encoding,&new_line,&on_error);
    FileStream* slots=(FileStream*)allocate(sizeof(*slots));
    slots->objcount=4;
    slots->pathname=pathname;
    slots->element_type=element_type;
    slots->external_format=external_format;
    slots->string=NIL_Symbol;
    slots->is_open=0;
    slots->direction=direction_input;
    slots->type=type;
    slots->encoding=encoding;
    slots->new_line=new_line;
    slots->on_error=on_error;
    slots->stream=NULL;
    return Object_new(find_class(symbol_intern_cstring("FILE-STREAM")),
                      (Slots*)slots);}

Ref string_stream_new(FILE* memstream){
    Ref element_type=S(CHARACTER);
    Ref external_format=list(K(ISO-8859-1),K(LF),S(NIL));
    type_t type=type_character;
    encoding_t encoding=encoding_iso_8859_1;
    new_line_t new_line=new_line_lf;
    on_error_t on_error=on_error_ignore;
    FileStream* slots=(FileStream*)allocate(sizeof(*slots));
    slots->objcount=4;
    slots->pathname=NIL_Symbol;
    slots->element_type=element_type;
    slots->external_format=external_format;
    slots->string=NIL_Symbol;
    slots->is_open=1;
    slots->direction=direction_input;
    slots->type=type;
    slots->encoding=encoding;
    slots->new_line=new_line;
    slots->on_error=on_error;
    slots->stream=memstream;
    return Object_new(find_class(symbol_intern_cstring("STRING-STREAM")),
                      (Slots*)slots);}

Ref stream_input_string(Ref string,Ref start,Ref end){
    uword len   = string_length(string);
    word slen   = CHECK_UWORD_TO_WORD(len);
    word istart = (nullp(start)) ?0 : integer_value(start);
    word iend   = (nullp(end)) ? slen : integer_value(end);
    if((istart<0) || (slen<istart)){
        ERROR("START = %"WORD_FORMAT" is beyond length of string = %"WORD_FORMAT"",istart,len);}
    if(slen<iend){
        ERROR("END = %"WORD_FORMAT" is beyond length of string = %"WORD_FORMAT"",iend,slen);}
    if(iend<istart){
        ERROR("END = %"WORD_FORMAT" is before START = %"WORD_FORMAT"",iend,istart);}
    char* buffer = (char*)string_octets(string)+istart;
    size_t size = CHECK_WORD_TO_SIZE(iend-istart);
    FILE* memstream=fmemopen(buffer,size,"r");
    if(!memstream){
        ERROR("Cannot create the STRING-STREAM for a string of length %"WORD_FORMAT"",size);}
    Ref stream=string_stream_new(memstream);
    stream->slots->file_stream.direction=direction_input;
    stream->slots->file_stream.string=string;
    stream->slots->file_stream.buffer=(octet*)buffer;
    stream->slots->file_stream.buffer_size=size;
    return stream;}

Ref stream_output_string(unused Ref element_type){
    /* Element type is not that useful (our BASE-CHAR = CHARACTER), but it's in the API. */
    Ref string=string_new(STRING_STREAM_BUFFER_SIZE);
    char* buffer = (char*)string_octets(string);
    size_t size = STRING_STREAM_BUFFER_SIZE;
    FILE* memstream=fmemopen(buffer,size,"w");
    if(!memstream){
        ERROR("Cannot create the STRING-STREAM for a string of length %"WORD_FORMAT"",size);}
    Ref stream=string_stream_new(memstream);
    stream->slots->file_stream.direction=direction_output;
    stream->slots->file_stream.string=string;
    stream->slots->file_stream.buffer=(octet*)buffer;
    string->slots->octet_array.dimensions[0]=CHECK_SIZE_TO_UWORD(size);
    return stream;}

Ref string_output_stream_get_string(Ref string_output_stream){
    FILE* cstream=string_output_stream->slots->file_stream.stream;
    fflush(cstream);
    long pos=ftell(cstream);
    /* int err=errno; */
    if(pos<0){
        STREAM_ERROR(string_output_stream); /* ,"OS Stream Error %d",err); */}
    Ref string=string_output_stream->slots->file_stream.string;
    string->slots->octet_array.dimensions[0]=CHECK_LONG_TO_UWORD(pos);
    return string;}

Ref stream_pathname(Ref stream){
    check_type(stream,FILE_STREAM_Class);
    return stream->slots->file_stream.pathname;}

Ref stream_element_type(Ref stream){
    check_type(stream,FILE_STREAM_Class);
    return stream->slots->file_stream.element_type;}

Ref stream_external_format(Ref stream){
    check_type(stream,FILE_STREAM_Class);
    return stream->slots->file_stream.external_format;}

Ref stream_direction(Ref stream){
    check_type(stream,FILE_STREAM_Class);
    return convert_from_direction(stream->slots->file_stream.direction);}

bool stream_open_p(Ref stream){
    check_type(stream,FILE_STREAM_Class);
    return stream->slots->file_stream.is_open;}

FILE* stream_cstream(Ref stream){
    check_type(stream,FILE_STREAM_Class);
    return stream->slots->file_stream.stream;}

type_t stream_type(Ref stream){
    check_type(stream,FILE_STREAM_Class);
    return stream->slots->file_stream.type;}


Ref standard_input_stream(void){
    Ref stream=file_stream_new(physical_pathname_new_cstring("/dev/stdin"),
                                  S(CHARACTER),
                                  K(DEFAULT));
    stream->slots->file_stream.is_open=true;
    stream->slots->file_stream.direction=direction_input;
    stream->slots->file_stream.stream=stdin;
    return stream;}

Ref standard_output_stream(void){
    Ref stream=file_stream_new(physical_pathname_new_cstring("/dev/stdout"),
                                  S(CHARACTER),
                                  K(DEFAULT));
    stream->slots->file_stream.is_open=true;
    stream->slots->file_stream.direction=direction_output;
    stream->slots->file_stream.stream=stdout;
    return stream;}

Ref standard_error_stream(void){
    Ref stream=file_stream_new(physical_pathname_new_cstring("/dev/stderr"),
                                  S(CHARACTER),
                                  K(DEFAULT));
    stream->slots->file_stream.is_open=true;
    stream->slots->file_stream.direction=direction_output;
    stream->slots->file_stream.stream=stderr;
    return stream;}

/* ENVIRONMENT */

static Environment* Environment_slots(Ref next_environment,bool is_toplevel){
    Environment* that=allocate(sizeof(*that));
    that->objcount=6;
    that->variables        = NIL_Symbol;
    that->functions        = NIL_Symbol;
    that->blocks           = NIL_Symbol;
    that->catchs           = NIL_Symbol;
    that->declarations     = NIL_Symbol;
    that->next_environment = next_environment;
    that->frames=NULL;
    that->is_toplevel=is_toplevel;
    return that;}

Ref environment_new(Ref next_environment,Ref toplevelp){
    return Object_new(ENVIRONMENT_Class,(Slots*)Environment_slots(next_environment,!nullp(toplevelp)));}


/* FUNCTION */

Closure* Closure_slots(Ref function,Ref environment){
    check_type(function,FUNCTION_Class);
    check_type(environment,ENVIRONMENT_Class);
    Closure* that=allocate(sizeof(*that));
    that->objcount=2;
    that->function=function;
    that->environment=environment;
    return that;}

Ref closure_new(Ref function,Ref environment){
    return Object_new(CLOSURE_Class,(Slots*)Closure_slots(function,environment));}


Function* Function_slots(Ref lisp_name,Ref lambda_list,Ref body){
    check_type(lisp_name,SYMBOL_Class);
    check_type(lambda_list,LIST_Class);
    check_type(body,LIST_Class);
    Function* that=allocate(sizeof(*that));
    that->objcount=3;
    that->lisp_name=lisp_name;
    that->lambda_list=lambda_list;
    that->body=body;
    return that;}

Ref function_new(Ref lisp_name,Ref lambda_list,Ref body){
    return Object_new(FUNCTION_Class,(Slots*)Function_slots(lisp_name,lambda_list,body));}

BuiltInFunction* BuiltInFunction_slots(Ref lisp_name,Ref lambda_list,KernelFunctionPtr kernel_function){
    check_type(lisp_name,SYMBOL_Class);
    check_type(lambda_list,LIST_Class);
    BuiltInFunction* that=allocate(sizeof(*that));
    that->objcount=2;
    that->lisp_name=lisp_name;
    that->lambda_list=lambda_list;
    that->kernel_function=kernel_function;
    return that;}

Ref built_in_function_new(Ref lisp_name,Ref lambda_list,KernelFunctionPtr kernel_function){
    return Object_new(BUILT_IN_FUNCTION_Class,(Slots*)BuiltInFunction_slots(lisp_name,lambda_list,kernel_function));}

//
// typedef struct FuncallableStandardObject {
//     BLOCK_HEADER;
//     /* TODO: FuncallableStandardObject */
//     Ref           object; /* The object data */
//     KernelFunctionPtr apply;  /* The entry point of to the Funcallable Standard Object. */
// } FuncallableStandardObject;

bool closurep(Ref object){
    return class_of(object)==CLOSURE_Class;}

bool functionp(Ref object){
    return typep(object,FUNCTION_Class);}

bool built_in_function_p(Ref object){
    return class_of(object)==BUILT_IN_FUNCTION_Class;}

Ref function_name(Ref funclo){
    if(closurep(funclo)){
        return function_name(funclo->slots->closure.function);}
    check_type(funclo,FUNCTION_Class);
    return funclo->slots->function.lisp_name;}

void values(Ref arguments,MultipleValues* results){
    results->count=0;
    while(consp(arguments) && (results->count<MULTIPLE_VALUES_LIMIT)){
        results->values[results->count++]=car(arguments);}}

void kernel_apply(Ref function_or_closure,Ref arguments,MultipleValues* results){
    if(closurep(function_or_closure)){
        kernel_apply_closure(function_or_closure,&function_or_closure->slots->closure,arguments,results);}
    else if(built_in_function_p(function_or_closure)){
        kernel_apply_built_in(function_or_closure,&function_or_closure->slots->built_in_function,arguments,results);}
    else if(functionp(function_or_closure)){
        kernel_apply_function(function_or_closure,&function_or_closure->slots->function,arguments,results);}
    else{
        /* TODO: (or function closure) */
        TYPE_ERROR(function_or_closure,FUNCTION_Class);}}

void kernel_apply_closure(Ref closure,Closure* slots,Ref arguments,MultipleValues* results){
    NOT_IMPLEMENTED_YET();
    /* slots->environment */
    /* slots->function */
    /*         (closure,arguments,results); */}

void kernel_apply_function(Ref function,Function* slots,Ref arguments,MultipleValues* results){
    NOT_IMPLEMENTED_YET();
    /* slots->body(function,arguments,results); */}

void kernel_apply_built_in(Ref function,BuiltInFunction* slots,Ref arguments,MultipleValues* results){
    slots->kernel_function(function,arguments,results);}


/* CONDITION */
/* STANDARD-OBJECT */


void check_no_more_arguments(Ref function,Ref arguments){
    if(nullp(arguments)){
        return;}
    char* fname=string_cstring(function_name(function));
    char* args=string_cstring(prin1_to_string(arguments));
    error(fname,"Too many arguments, unused: %s",args);
    /* TODO: clear up this mess, error doesn't return! */
    free(fname);free(args);}

Ref pop_mandatory_argument(Ref function,Ref* arguments){
    if(nullp(*arguments)){
        char* fname=string_cstring(function_name(function));
        error(fname,"Missing mantadory argument");
        /* TODO: clear up this mess, error doesn't return! */
        free(fname);}
    return pop(arguments);}

Ref pop_optional_argument(unused Ref function,Ref* arguments,Ref default_value){
    return nullp(*arguments)
            ? default_value
            : pop(arguments);}

Ref pop_key_argument(Ref function,Ref* arguments,Ref keyword,Ref default_value){
    Ref current=(*arguments);
    while(consp(current) && !eql(keyword,car(current))){
        if(consp(cdr(current))){
            current=cddr(current);}
        else{
            char* fname=string_cstring(function_name(function));
            char* args=string_cstring(prin1_to_string(*arguments));
            char* mkey=string_cstring(prin1_to_string(keyword));
            error(fname,"Odd number of &key arguments %s, missing value for %s",
                  args,mkey);
            /* TODO: clear up this mess, error doesn't return! */
            free(fname);free(args);free(mkey);}}
    return consp(current)
            ? cadr(current)
            : default_value;}


static Ref rootset;
static Ref kernel_initialize_rootset(void){
    rootset=NIL_Symbol;
    push(UNBOUND_Object,&rootset);
    push(KERNEL_obarray,&rootset);
    push(KEYWORD_obarray,&rootset);
    push(kernel_classes,&rootset);
    push(characterset,&rootset);
    return rootset;}

Ref kernel_rootset(void){
    return rootset;}

void push_to_rootset(Ref object){
    rootset=cons(object,rootset);}

void delete_from_rootset(Ref object){
    rootset=delete(object,rootset);}


static void kernel_initialize_features(void){
    Ref features=S("*FEATURES*");
    Ref list=NIL_Symbol;

    push(K(COMMON-LISP),&list);
    push(K(BOCL),&list);
    push(K(BOCL-1.0),&list);

#ifdef _POSIX_V6_LP64_OFF64
    push(K(64-BIT-TARGET),&list);
    push(K(64-BIT-HOST),&list);
#else
    push(K(32-BIT-TARGET),&list);
    push(K(32-BIT-HOST),&list);
#endif

    define_parameter(features,list);

    define_constant(S(ARRAY-TOTAL-SIZE-LIMIT),  I(ARRAY_TOTAL_SIZE_LIMIT));
    define_constant(S(ARRAY-DIMENSION-LIMIT),   I(ARRAY_DIMENSION_LIMIT));
    define_constant(S(ARRAY-RANK-LIMIT),        I(ARRAY_RANK_LIMIT));
    define_constant(S(CALL-ARGUMENTS-LIMIT),    I(CALL_ARGUMENTS_LIMIT));
    define_constant(S(CHAR-CODE-LIMIT),         I(CHAR_CODE_LIMIT));
    define_constant(S(LAMBDA-PARAMETERS-LIMIT), I(LAMBDA_PARAMETERS_LIMIT));
    define_constant(S(MULTIPLE-VALUES-LIMIT),   I(MULTIPLE_VALUES_LIMIT));

    define_variable(S(*LOADING-FASL-FILE*),     S(NIL));
}

void kernel_initialize(void){
    memory_initialize();
    set_garbage_collection_enabled(false);

    kernel_objects_initialize();
    kernel_initialize_built_in_functions();
    kernel_initialize_integers();
    kernel_initialize_characters();
    kernel_initialize_features();

    structure_initialize(); /* before: */
    kernel_initialize_structure_functions();

    reader_initialize();
    stream_initialize();

    kernel_initialize_rootset();
    set_rootset((Block*)kernel_rootset());
    set_garbage_collection_enabled(true);}



/**** THE END ****/

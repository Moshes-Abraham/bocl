#ifndef character_h
#define character_h
#include "kernel_types.h"

bool printablep(octet ch);
bool constituentp(octet ch);
bool whitespacep(octet character);

#endif
